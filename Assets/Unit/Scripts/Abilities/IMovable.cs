using UnityEngine;
using UnityEngine.AI;

namespace TroopControl.Units
{
    public interface IMovable
    {
        public NavMeshAgent NavMeshAgent { get; }
        public Animator Animator { get; }

        public Transform Target { get; set; }

        public float MoveSpeed { get; }
        public float TargetDistanceThreshold { get; }

        public State IdleState { get; }
        public State MoveState { get; }
    }
}
