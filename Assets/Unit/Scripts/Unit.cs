using UnityEngine;
using TroopControl.Selection;

namespace TroopControl.Units
{
    public abstract class Unit : MonoBehaviour, ISelectable
    {
        [SerializeField] protected UnitManager unitManager;

        [SerializeField] protected new string name;
        public string Name => name;

        [SerializeField] protected GameObject selectionIndicator;
        protected bool selectionIndicatorVisible;

        protected State currentState;

        public bool IsSelected { get; set; }

        protected virtual void Start()
        {
            selectionIndicator.SetActive(false);
        }

        protected virtual void Update()
        {
            currentState.OnExecute();

            if(IsSelected && !selectionIndicatorVisible)
            {
                selectionIndicator.SetActive(true);
                selectionIndicatorVisible = true;
            }
            else if(!IsSelected && selectionIndicatorVisible)
            {
                selectionIndicator.SetActive(false);
                selectionIndicatorVisible = false;
            }
        }

        public virtual void ChangeState(State nextState)
        {
            if(nextState != null)
            {
                currentState.OnExit();

                currentState = nextState;

                currentState.OnEnter();
            }
        }
    }
}