using UnityEngine;
using UnityEngine.AI;

namespace TroopControl.Units
{
    public class Infantry : Unit, IMovable
    {
        [Space]
        [SerializeField] private NavMeshAgent navMeshAgent;
        public NavMeshAgent NavMeshAgent => navMeshAgent;
        [SerializeField] private Animator animator;
        public Animator Animator => animator;

        [Space]
        [SerializeField] private Transform target;
        public Transform Target { get => target; set => target = value; }

        private float moveSpeed => unitManager.InfantryStats.MoveSpeed;
        public float MoveSpeed => moveSpeed;

        private float targetDistanceThreshold => unitManager.InfantryStats.TargetDistanceThreshold;
        public float TargetDistanceThreshold => targetDistanceThreshold;

        [Space]
        [SerializeField] private State idleState;
        public State IdleState => idleState;
        [SerializeField] private State moveState;
        public State MoveState => moveState;

        protected override void Start()
        {
            base.Start();

            currentState = idleState;
        }
    }
}