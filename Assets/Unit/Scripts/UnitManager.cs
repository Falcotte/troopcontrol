using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TroopControl.Units
{
    public class UnitManager : MonoBehaviour
    {
        [SerializeField] private List<Unit> units;
        public List<Unit> Units => units;

        [SerializeField] private List<Unit> selectedUnits = new List<Unit>();
        public List<Unit> SelectedUnits => selectedUnits;

        [SerializeField] private Stats infantryStats;
        public Stats InfantryStats => infantryStats;

        [SerializeField] private Stats sniperStats;
        public Stats SniperStats => sniperStats;

        [SerializeField] private Stats heavyStats;
        public Stats HeavyStats => heavyStats;

        public int GetUnitCount(string unitName)
        {
            return selectedUnits.FindAll(x => x.Name == unitName).Count;
        }
    }
}