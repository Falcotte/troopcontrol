using UnityEngine;

namespace TroopControl.Units
{
    [CreateAssetMenu(fileName = "Stats", menuName = "TroopControl/Units/Stats")]
    public class Stats : ScriptableObject
    {
        [SerializeField] [Range(2f, 6f)] private float moveSpeed;
        public float MoveSpeed { get => moveSpeed; set => moveSpeed = value; }
        [SerializeField] private float targetDistanceThreshold;
        public float TargetDistanceThreshold { get => targetDistanceThreshold; set => targetDistanceThreshold = value; }
    }
}