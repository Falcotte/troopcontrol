using UnityEngine;

namespace TroopControl.Units
{
    public class IdleState : State
    {
        private IMovable movable;

        protected override void Awake()
        {
            base.Awake();

            movable = unit.GetComponent<IMovable>();
        }

        public override void OnEnter()
        {

        }

        public override void OnExecute()
        {
            if(movable == null) return;

            if(movable.Target)
            {
                if(Vector3.SqrMagnitude(transform.position - movable.Target.position) > movable.TargetDistanceThreshold)
                {
                    unit.ChangeState(movable.MoveState);
                }
            }
        }

        public override void OnExit()
        {

        }
    }
}