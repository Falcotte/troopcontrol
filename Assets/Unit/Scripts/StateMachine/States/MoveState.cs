using UnityEngine;

namespace TroopControl.Units
{
    public class MoveState : State
    {
        private IMovable movable;

        protected override void Awake()
        {
            base.Awake();

            movable = unit.GetComponent<IMovable>();
        }

        public override void OnEnter()
        {
            if(movable == null) return;

            movable.NavMeshAgent.isStopped = false;
            movable.Animator.SetBool("IsMoving", true);
        }

        public override void OnExecute()
        {
            if(movable == null) return;

            movable.NavMeshAgent.speed = movable.MoveSpeed;
            movable.Animator.SetFloat("MoveSpeed", Mathf.InverseLerp(2f, 6f, movable.MoveSpeed));
            if(movable.Target)
            {
                if(Vector3.SqrMagnitude(transform.position - movable.Target.position) <= movable.TargetDistanceThreshold)
                {
                    unit.ChangeState(movable.IdleState);
                }
                else
                {
                    Move(movable.Target.position);
                }
            }
            else
            {
                unit.ChangeState(movable.IdleState);
            }
        }

        public override void OnExit()
        {
            if(movable == null) return;

            movable.NavMeshAgent.isStopped = true;
            movable.Animator.SetBool("IsMoving", false);
        }

        private void Move(Vector3 moveTarget)
        {
            movable.NavMeshAgent.SetDestination(moveTarget);
        }
    }
}