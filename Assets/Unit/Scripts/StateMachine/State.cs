using UnityEngine;

namespace TroopControl.Units
{
    [RequireComponent(typeof(Unit))]
    public abstract class State : MonoBehaviour
    {
        protected Unit unit;

        protected virtual void Awake()
        {
            unit = GetComponent<Unit>();
        }

        public abstract void OnEnter();

        public abstract void OnExecute();

        public abstract void OnExit();
    }
}