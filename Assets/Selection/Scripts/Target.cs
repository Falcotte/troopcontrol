using UnityEngine;
using UnityEngine.Events;
using TroopControl.Selection;

public class Target : MonoBehaviour, ISelectable
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private float targetPositionThreshold;

    [SerializeField] private Vector2 xPositionLimits;
    [SerializeField] private Vector2 zPositionLimits;

    private Vector3 targetPosition;

    public bool IsSelected { get; set; }
    public bool HasMoved { get; set; }

    private void Start()
    {
        SetTargetPosition(transform.position);
    }

    private void Update()
    {
        if(Vector3.SqrMagnitude(transform.position - targetPosition) >= targetPositionThreshold)
        {
            MoveTowardsTargetPosition();
        }
        else
        {
            transform.position = targetPosition;
        }
    }

    private void MoveTowardsTargetPosition()
    {
        transform.position = Vector3.Lerp(transform.position, targetPosition, moveSpeed * Time.deltaTime);
    }

    public void SetTargetPosition(Vector3 position)
    {
        position = new Vector3(Mathf.Clamp(position.x, xPositionLimits.x, xPositionLimits.y), 0f, Mathf.Clamp(position.z, zPositionLimits.x, zPositionLimits.y));

        targetPosition = position;
    }
}
