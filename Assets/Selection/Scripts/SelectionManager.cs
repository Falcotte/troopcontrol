using System.Collections.Generic;
using UnityEngine;
using TroopControl.Units;
using TroopControl.UI;

namespace TroopControl.Selection
{
    public class SelectionManager : MonoBehaviour
    {
        [SerializeField] private UnitManager unitManager;

        [Space]
        [SerializeField] private Camera mainCamera;
        [SerializeField] private UIController uIController;

        [Space]
        [SerializeField] private SelectionBox selectionBox;
        [SerializeField] private float selectionBoxPaddingX;
        [SerializeField] private float selectionBoxPaddingZ;

        private Target selectedTarget;
        private List<Units.Unit> selectedUnits => unitManager.SelectedUnits;
        public List<Units.Unit> SelectedUnits => selectedUnits;

        private Vector2 initialBoxSelectionPosition;
        private Vector2 currentBoxSelectionPosition;

        private Plane touchWorldPlane;

        private Ray initialUnitSelectionRay;
        private Ray touchWorldRay;

        private Vector3 initialTouchWorldPosition;
        public Vector3 TouchWorldPosition { get; private set; }

        private bool shiftKeyPressed => Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
        public bool ShiftKeyPressed => shiftKeyPressed;

        // For debug purposes
        [Space]
        [SerializeField] private bool showRay;

        private void Start()
        {
            SetTouchWorldPlane();
        }

        private void Update()
        {
            HandleTargetInput();
            HandleUnitInput();
        }

        #region Raycast

        private void SetTouchWorldPlane()
        {
            touchWorldPlane = new Plane(Vector3.up, Vector3.zero);
        }

        public void FireTouchWorldRay()
        {
            touchWorldRay = mainCamera.ScreenPointToRay(Input.mousePosition);

#if UNITY_EDITOR
            if(showRay)
            {
                Debug.DrawRay(mainCamera.transform.position, touchWorldRay.direction * 1000, Color.red, 1f);
            }
#endif
        }

        public void FireInitialUnitSelectionRay()
        {
            initialUnitSelectionRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        }

        // Sets the position as the intersection of the ray and the plane
        private void SetTouchWorldPosition()
        {
            float enter;

            if(touchWorldPlane.Raycast(touchWorldRay, out enter))
            {
                TouchWorldPosition = touchWorldRay.GetPoint(enter);

                if(selectedTarget != null && ((Vector3.SqrMagnitude(TouchWorldPosition - initialTouchWorldPosition) >= .05f) || selectedTarget.HasMoved))
                {
                    selectedTarget.HasMoved = true;
                    selectedTarget.SetTargetPosition(TouchWorldPosition);
                }
            }
        }

        private void SetInitialTouchWorldPosition()
        {
            float enter;

            if(touchWorldPlane.Raycast(initialUnitSelectionRay, out enter))
            {
                initialTouchWorldPosition = initialUnitSelectionRay.GetPoint(enter);
            }
        }

        #endregion

        #region Target Selection

        private void HandleTargetInput()
        {
            if(Input.GetMouseButtonDown(0))
            {
                FireTouchWorldRay();
                SelectTarget();
                FireInitialUnitSelectionRay();
                SetInitialTouchWorldPosition();
            }

            if(Input.GetMouseButton(0))
            {
                FireTouchWorldRay();
                SetTouchWorldPosition();
            }

            if(Input.GetMouseButtonUp(0))
            {
                if(selectedTarget != null)
                {
                    selectedTarget.IsSelected = false;
                    selectedTarget.HasMoved = false;
                    selectedTarget = null;
                }
            }
        }

        private void SelectTarget()
        {
            RaycastHit targetHit;

            if(Physics.Raycast(touchWorldRay, out targetHit, 1000, LayerMask.GetMask("Target")))
            {
                selectedTarget = targetHit.transform.GetComponent<Target>();

                if(selectedTarget != null)
                {
                    selectedTarget.IsSelected = true;

                    if(selectedUnits.Count > 0)
                    {
                        foreach(var selectedUnit in selectedUnits)
                        {
                            selectedUnit.GetComponent<Units.IMovable>().Target = selectedTarget.transform;
                        }
                    }
                }
            }
        }

        #endregion

        #region Unit Selection

        private void HandleUnitInput()
        {
            if(Input.GetMouseButtonDown(1))
            {
                FireTouchWorldRay();
                FireInitialUnitSelectionRay();
                SetInitialTouchWorldPosition();
                SelectUnit();

                initialBoxSelectionPosition = Input.mousePosition;
                selectionBox.gameObject.SetActive(true);
                uIController.SetSelectionBoxStatus(true);
            }

            if(Input.GetMouseButton(1))
            {
                FireTouchWorldRay();
                SetTouchWorldPosition();

                currentBoxSelectionPosition = Input.mousePosition;
                uIController.AdjustSelectionBox(initialBoxSelectionPosition, currentBoxSelectionPosition);
                SetSelectionBox();
            }

            if(Input.GetMouseButtonUp(1))
            {
                if(selectedTarget != null)
                {
                    selectedTarget.IsSelected = false;
                    selectedTarget = null;
                }

                uIController.SetSelectionBoxStatus(false);
                selectionBox.gameObject.SetActive(false);
            }
        }

        private void SelectUnit()
        {
            RaycastHit unitHit;

            if(Physics.Raycast(touchWorldRay, out unitHit, 1000, LayerMask.GetMask("Unit")))
            {
                Units.Unit unit = unitHit.transform.GetComponent<Units.Unit>();

                if(unit != null)
                {
                    if(!selectedUnits.Contains(unit))
                    {
                        if(!shiftKeyPressed)
                        {
                            foreach(var selectedUnit in selectedUnits)
                            {
                                selectedUnit.IsSelected = false;
                            }
                            selectedUnits.Clear();

                            selectedUnits.Add(unit);
                            unit.IsSelected = true;
                        }
                        else
                        {
                            selectedUnits.Add(unit);
                            unit.IsSelected = true;
                        }
                    }
                    else
                    {
                        if(!shiftKeyPressed)
                        {
                            foreach(var selectedUnit in selectedUnits)
                            {
                                selectedUnit.IsSelected = false;
                            }
                            selectedUnits.Clear();
                        }
                    }
                }
            }
            else
            {
                if(!shiftKeyPressed)
                {
                    foreach(var selectedUnit in selectedUnits)
                    {
                        selectedUnit.IsSelected = false;
                    }
                    selectedUnits.Clear();
                }
            }
        }

        private void SetSelectionBox()
        {
            selectionBox.transform.position = new Vector3((initialTouchWorldPosition.x + TouchWorldPosition.x) / 2f, 1f, (initialTouchWorldPosition.z + TouchWorldPosition.z) / 2f);

            selectionBox.transform.rotation = Quaternion.Euler(0f, mainCamera.transform.eulerAngles.y, 0f);

            Vector3 initialPos = selectionBox.transform.InverseTransformPoint(initialTouchWorldPosition);
            Vector3 currentPos = selectionBox.transform.InverseTransformPoint(TouchWorldPosition);

            selectionBox.BoxCollider.size = new Vector3(Mathf.Abs(initialPos.x - currentPos.x) + selectionBoxPaddingX, 1f, Mathf.Abs(initialPos.z - currentPos.z + Mathf.Tan(Vector3.Angle(Vector3.down, mainCamera.transform.forward) * Mathf.Deg2Rad)) + selectionBoxPaddingZ);
        }

        #endregion
    }
}
