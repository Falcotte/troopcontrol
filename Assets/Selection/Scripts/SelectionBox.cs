using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TroopControl.Selection
{
    public class SelectionBox : MonoBehaviour
    {
        [SerializeField] private SelectionManager selectionManager;

        [SerializeField] private BoxCollider boxCollider;
        public BoxCollider BoxCollider => boxCollider;

        private void Start()
        {
            gameObject.SetActive(false);
        }

        private void OnTriggerEnter(Collider other)
        {
            Units.Unit unit = other.GetComponent<Units.Unit>();
            if(unit != null && !unit.IsSelected)
            {
                if(!selectionManager.SelectedUnits.Contains(unit))
                {
                    selectionManager.SelectedUnits.Add(unit);
                    unit.IsSelected = true;
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            Units.Unit unit = other.GetComponent<Units.Unit>();
            if(unit != null && unit.IsSelected)
            {
                if(selectionManager.SelectedUnits.Contains(unit) && !selectionManager.ShiftKeyPressed)
                {
                    selectionManager.SelectedUnits.Remove(unit);
                    unit.IsSelected = false;
                }
            }
        }
    }
}