namespace TroopControl.Selection
{
    public interface ISelectable
    {
        public bool IsSelected { get; set; }
    }
}
