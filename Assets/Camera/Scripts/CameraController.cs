using UnityEngine;

namespace TroopControl.CameraControls
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private Vector2 xPositionLimits;
        [SerializeField] private Vector2 zPositionLimits;

        [SerializeField] private float moveSpeed;

        [Space]
        [SerializeField] [Range(40f, 60f)] private float verticalMinRotation;
        [SerializeField] [Range(60f, 88f)] private float verticalMaxRotation;

        [SerializeField] private float rotationSpeed;

        [Space]
        [SerializeField] private bool invertVerticalLook;

        private Vector3 cameraRight;
        private Vector3 cameraForward;

        private Vector3 initialMousePosition;
        private Vector3 deltaMousePosition;

        void Update()
        {
            AdjustCameraPosition();

            if(Input.GetMouseButtonDown(2))
            {
                initialMousePosition = Input.mousePosition;
            }

            AdjustCameraRotation();
        }

        private void AdjustCameraPosition()
        {
            cameraRight = transform.right.normalized;
            cameraForward = new Vector3(transform.forward.x, 0f, transform.forward.z).normalized;

            transform.position += (cameraRight * Input.GetAxis("Horizontal") + cameraForward * Input.GetAxis("Vertical")) * moveSpeed * Time.deltaTime;
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, xPositionLimits.x, xPositionLimits.y),
                transform.position.y,
                Mathf.Clamp(transform.position.z, zPositionLimits.x, zPositionLimits.y));
        }

        private void AdjustCameraRotation()
        {
            if(Input.GetMouseButton(2))
            {
                deltaMousePosition = Input.mousePosition - initialMousePosition;

                Vector3 v = transform.localRotation.eulerAngles;
                if(invertVerticalLook)
                {
                    transform.localRotation = Quaternion.Euler(Mathf.Clamp(v.x + (deltaMousePosition.y / Screen.height) * rotationSpeed * Time.deltaTime, verticalMinRotation, verticalMaxRotation),
                        v.y + (deltaMousePosition.x / Screen.width) * rotationSpeed * Time.deltaTime,
                        v.z);
                }
                else
                {
                    transform.localRotation = Quaternion.Euler(Mathf.Clamp(v.x - (deltaMousePosition.y / Screen.height) * rotationSpeed * Time.deltaTime, verticalMinRotation, verticalMaxRotation),
                        v.y + (deltaMousePosition.x / Screen.width) * rotationSpeed * Time.deltaTime,
                        v.z);
                }
            }
        }
    }
}