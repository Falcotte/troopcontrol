using UnityEngine;
using UnityEngine.UI;
using TroopControl.Units;

namespace TroopControl.UI
{
    public class StatsPanel : MonoBehaviour
    {
        [SerializeField] private Stats infantryStats;
        [SerializeField] private Stats sniperStats;
        [SerializeField] private Stats heavyStats;

        [SerializeField] private Slider infantryMoveSpeed;
        [SerializeField] private Slider infantryMovementThreshold;
        [SerializeField] private Slider sniperMoveSpeed;
        [SerializeField] private Slider sniperMovementThreshold;
        [SerializeField] private Slider heavyMoveSpeed;
        [SerializeField] private Slider heavyMovementThreshold;

        private void Start()
        {
            SetSpeedSliders();
            SetThresholdSliders();
        }

        private void SetSpeedSliders()
        {
            infantryMoveSpeed.minValue = 2f;
            infantryMoveSpeed.maxValue = 6f;
            infantryMoveSpeed.value = infantryStats.MoveSpeed;
            infantryMoveSpeed.onValueChanged.AddListener(delegate
            {
                SetMoveSpeed();
            });
            sniperMoveSpeed.minValue = 2f;
            sniperMoveSpeed.maxValue = 6f;
            sniperMoveSpeed.value = sniperStats.MoveSpeed;
            sniperMoveSpeed.onValueChanged.AddListener(delegate
            {
                SetMoveSpeed();
            });
            heavyMoveSpeed.minValue = 2f;
            heavyMoveSpeed.maxValue = 6f;
            heavyMoveSpeed.value = heavyStats.MoveSpeed;
            heavyMoveSpeed.onValueChanged.AddListener(delegate
            {
                SetMoveSpeed();
            });
        }

        private void SetThresholdSliders()
        {
            infantryMovementThreshold.minValue = 5f;
            infantryMovementThreshold.maxValue = 20f;
            infantryMovementThreshold.value = infantryStats.TargetDistanceThreshold;
            infantryMovementThreshold.onValueChanged.AddListener(delegate
            {
                SetMoveThreshold();
            });
            sniperMovementThreshold.minValue = 5f;
            sniperMovementThreshold.maxValue = 20f;
            sniperMovementThreshold.value = sniperStats.TargetDistanceThreshold;
            sniperMovementThreshold.onValueChanged.AddListener(delegate
            {
                SetMoveThreshold();
            });
            heavyMovementThreshold.minValue = 5f;
            heavyMovementThreshold.maxValue = 20f;
            heavyMovementThreshold.value = heavyStats.TargetDistanceThreshold;
            heavyMovementThreshold.onValueChanged.AddListener(delegate
            {
                SetMoveThreshold();
            });
        }

        private void SetMoveSpeed()
        {
            infantryStats.MoveSpeed = infantryMoveSpeed.value;
            sniperStats.MoveSpeed = sniperMoveSpeed.value;
            heavyStats.MoveSpeed = heavyMoveSpeed.value;
        }

        private void SetMoveThreshold()
        {
            infantryStats.TargetDistanceThreshold = infantryMovementThreshold.value;
            sniperStats.TargetDistanceThreshold = sniperMovementThreshold.value;
            heavyStats.TargetDistanceThreshold = heavyMovementThreshold.value;
        }
    }
}
