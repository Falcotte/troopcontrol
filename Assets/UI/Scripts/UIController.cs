using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TroopControl.Units;

namespace TroopControl.UI
{
    public class UIController : MonoBehaviour
    {
        [SerializeField] private UnitManager unitManager;

        [Space]
        [SerializeField] private Canvas canvas;
        [SerializeField] private RectTransform selectionBox;

        [Space]
        [SerializeField] private SelectionPanel selectionPanel;

        private void Update()
        {
            selectionPanel.SetUnitInfo(0, "Infantry", unitManager.GetUnitCount("Infantry"));
            selectionPanel.SetUnitInfo(1, "Sniper", unitManager.GetUnitCount("Sniper"));
            selectionPanel.SetUnitInfo(2, "Heavy", unitManager.GetUnitCount("Heavy"));

            selectionPanel.SetUnitInfoStatus();
        }

        public void SetSelectionBoxStatus(bool enabled)
        {
            if(!selectionBox.gameObject.activeInHierarchy && enabled)
            {
                selectionBox.gameObject.SetActive(true);
            }
            else if(selectionBox.gameObject.activeInHierarchy && !enabled)
            {
                selectionBox.gameObject.SetActive(false);
            }
        }

        public void AdjustSelectionBox(Vector2 initialPosition, Vector2 finalPosition)
        {
            Vector2 initialPosConverted;
            Vector2 finalPosConverted;

            RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, initialPosition, canvas.worldCamera, out initialPosConverted);
            RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, finalPosition, canvas.worldCamera, out finalPosConverted);

            selectionBox.transform.position = canvas.transform.TransformPoint((initialPosConverted + finalPosConverted) / 2f);

            float width = Mathf.Abs(initialPosConverted.x - finalPosConverted.x);
            float height = Mathf.Abs(initialPosConverted.y - finalPosConverted.y);
            selectionBox.sizeDelta = new Vector2(width, height);
        }

        public void SelectAllUnits()
        {
            foreach(Units.Unit unit in unitManager.Units)
            {
                if(!unitManager.SelectedUnits.Contains(unit))
                {
                    unitManager.SelectedUnits.Add(unit);
                    unit.IsSelected = true;
                }
            }
        }

        public void ClearTarget()
        {
            foreach(Units.Unit unit in unitManager.Units)
            {
                unit.GetComponent<IMovable>().Target = null;
            }
        }
    }
}