using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace TroopControl.UI
{
    public class SelectionPanel : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI selectedUnit0;
        [SerializeField] private TextMeshProUGUI selectedUnitCount0;
        [SerializeField] private TextMeshProUGUI selectedUnit1;
        [SerializeField] private TextMeshProUGUI selectedUnitCount1;
        [SerializeField] private TextMeshProUGUI selectedUnit2;
        [SerializeField] private TextMeshProUGUI selectedUnitCount2;

        int unit0Count;
        int unit1Count;
        int unit2Count;

        private void Start()
        {
            selectedUnit0.gameObject.SetActive(false);
            selectedUnitCount0.gameObject.SetActive(false);
            selectedUnit1.gameObject.SetActive(false);
            selectedUnitCount1.gameObject.SetActive(false);
            selectedUnit2.gameObject.SetActive(false);
            selectedUnitCount2.gameObject.SetActive(false);
        }

        public void SetUnitInfo(int index, string name, int count)
        {
            switch(index)
            {
                case 0:
                    selectedUnit0.text = name;
                    selectedUnitCount0.text = count.ToString();
                    unit0Count = count;
                    break;
                case 1:
                    selectedUnit1.text = name;
                    selectedUnitCount1.text = count.ToString();
                    unit1Count = count;
                    break;
                case 2:
                    selectedUnit2.text = name;
                    selectedUnitCount2.text = count.ToString();
                    unit2Count = count;
                    break;
            }
        }

        public void SetUnitInfoStatus()
        {
            if(unit0Count > 0)
            {
                selectedUnit0.gameObject.SetActive(true);
                selectedUnitCount0.gameObject.SetActive(true);
            }
            else
            {
                selectedUnit0.gameObject.SetActive(false);
                selectedUnitCount0.gameObject.SetActive(false);
            }
            if(unit1Count > 0)
            {
                selectedUnit1.gameObject.SetActive(true);
                selectedUnitCount1.gameObject.SetActive(true);
            }
            else
            {
                selectedUnit1.gameObject.SetActive(false);
                selectedUnitCount1.gameObject.SetActive(false);
            }
            if(unit2Count > 0)
            {
                selectedUnit2.gameObject.SetActive(true);
                selectedUnitCount2.gameObject.SetActive(true);
            }
            else
            {
                selectedUnit2.gameObject.SetActive(false);
                selectedUnitCount2.gameObject.SetActive(false);
            }
        }
    }
}